from dataclasses import dataclass, field
from datetime import datetime
from typing import Dict

import rich
from rich.console import Console


@dataclass
class ChatMessage:
    display_name: str = ""
    subscriber: int = 0
    user_type: str = ""
    mod: bool = False
    cmd: Dict = field(default_factory=dict)
    nick: str = ""
    body: str = ""
    color: str = "#f767c2"
    timestamp: str = "0"
    _console: Console = Console(color_system="truecolor")

    def handle_cmd(self):
        """respond to commands"""
        cmd = self.cmd.get("command")
        if cmd == "PRIVMSG":
            formatted_msg = self._format_chat_msg()
            rich.print(formatted_msg)

        elif cmd == "NOTICE":
            if "ROOMSTATE" not in self.body:
                rich.print(f"[INFO] - [italic #54f9d0]{self.body}[/italic #54f9d0]")

        elif cmd == "RECONNECT":
            rich.print(
                ":electric_plug:",
                f"[bold red]The server needs to terminate the connection.[/bold red]",
            )

        elif cmd == "USERNOTICE":
            rich.print(
                ":celebrate:",
                f"[bold #32a369]somebody probably subbed ![/bold #32a369]",
            )

        elif cmd == "WHISPER":
            rich.print(
                ":ninja:",
                f"[italic #c4c4c4]You received a whisper from a user. Go to twitch to reply.[/italic #c4c4c4]",
            )

        elif cmd == "GLOBALUSERSTATE":
            rich.print(
                ":white_check_mark:", "[bold green]We are authenticated[/bold green]"
            )

    def set_command(self, raw_cmd: str):
        cmds_to_ignore = [
            "CLEARCHAT",
            "CLEARMSG",
            "HOSTTARGET",
            "USERSTATE",
            "CAP",
            "ROOMSTATE",
            "001",
            "002",
            "003",
            "004",
            "353",
            "366",
            "372",
            "375",
            "376",
        ]

        cmd_received = raw_cmd.split(" ")
        if cmd_received[0] not in cmds_to_ignore:
            cmd_name = cmd_received[0]
            cmd_channel = cmd_received[1] if cmd_received[1] else None
            self.cmd.update({"command": cmd_name, "channel": cmd_channel})

    def set_nickname(self, raw_source: str):
        self.nick = raw_source.split("!")[0]

    def set_body(self, msg_body: str):
        self.body = msg_body

    def set_info(self, raw_tags: str):
        extracted_tags = {}
        tags_to_ignore = [
            "@badge-info",
            "badges",
            "client-nonce",
            "emotes",
            "flags",
            "id",
            "returning-chatter",
            "room-id",
            "first-msg",
            "turbo",
            "user-type",
            "user-id",
        ]
        parsed_tags = raw_tags.split(";")
        for tag in parsed_tags:
            parsed_tag = tag.split("=")
            tag_name = parsed_tag[0]
            tag_value = parsed_tag[1] if parsed_tag[1] != "" else None

            if tag_name not in tags_to_ignore:
                if tag_name == "tmi-sent-ts":
                    tag_name = "timestamp"
                    tag_value = self._format_timestamp(tag_value)
                new_tag = {tag_name.replace("-", "_"): tag_value}
                extracted_tags.update(new_tag)

        for tag, value in extracted_tags.items():
            if value is not None:
                setattr(self, tag, value)

    def _format_chat_msg(self) -> str:
        """
        returns the chat message send by the user
        in the following format:
        <emote|optional> <username|bold|color>: <message body>
        """
        return f"[#777777][{self.timestamp}][/#777777] [bold {self.color}]{self.nick}[/bold {self.color}]: {self.body}"

    def _format_timestamp(self, ts) -> str:
        return datetime.utcfromtimestamp(int(ts) / 1000).strftime("%H:%M")
