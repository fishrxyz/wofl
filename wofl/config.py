import configparser
import pathlib
import sys
from dataclasses import dataclass, field

from wofl.exceptions import MissingSectionError, MissingTokenError

# from wofl.exceptions import MissingSettingsSection


@dataclass
class WoflConfig:
    parser: configparser.ConfigParser
    server: str = ""
    port: int = 0
    token: str = ""
    nickname: str = ""
    capabilities: list = field(default_factory=list)
    path: str = "~/.config/wofl/config.ini"

    def _validate(self) -> None:
        """
        checks if there is a config at the designated default path
        """
        path = pathlib.Path(self.path).expanduser()
        if not path.is_file():
            print(f"[ERROR] - Could not find config file under {self.path}")
            sys.exit(1)

    def _parse(self) -> None:
        """
        Use configparser to read values from the user's config
        That file will need to exist at ~/.config/wofl/config
        """

        # 1. open the file
        self.parser.read(self.path)

        # 2. read the values and add possible defaults
        try:
            settings = self.parser["settings"]
        except KeyError:
            raise MissingSectionError(section="settings")
        else:
            self.server = settings.get("server")
            self.port = settings.getint("port")
            self.nickname = settings.get("nickname")
            self.capabilities = settings.get("capabilities").split(",")
            self.token = settings.get("token")

            if self.token is None:
                raise MissingTokenError

    def __post_init__(self):
        self.path = str(pathlib.Path(self.path).expanduser())
        self._validate()
        self._parse()
