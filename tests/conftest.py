import configparser
import socket

import pytest

from wofl.client import Wofl
from wofl.config import WoflConfig
from wofl.message import ChatMessage


@pytest.fixture
def chat_msg() -> ChatMessage:
    return ChatMessage(
        display_name="DeKooks", nick="ayodekooks", timestamp="1684336330162"
    )


@pytest.fixture
def parser() -> configparser.ConfigParser:
    return configparser.ConfigParser()


@pytest.fixture
def wofl(parser, mocker) -> Wofl:
    with mocker.patch("wofl.client.socket.socket"):
        cfg = WoflConfig(parser=parser)
        return Wofl(cfg=cfg)
