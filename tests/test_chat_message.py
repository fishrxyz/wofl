from wofl.message import ChatMessage

# NOTE: use this URL to learn more about PRIVMSG tags https://dev.twitch.tv/docs/irc/tags/#privmsg-tags


def test_format_timestamp(chat_msg: ChatMessage):
    ts = "1684336330162"  # timestamp corresponding to 15:12
    formatted_ts = chat_msg._format_timestamp(ts)
    assert formatted_ts == "15:12"


def test_format_chat_msg(chat_msg: ChatMessage):
    chat_msg.timestamp = "22:34"
    chat_msg.body = "ayo this is a test msg"
    formatted_msg = chat_msg._format_chat_msg()
    expected_msg = f"[#777777][{chat_msg.timestamp}][/#777777] [bold {chat_msg.color}]{chat_msg.nick}[/bold {chat_msg.color}]: {chat_msg.body}"
    assert formatted_msg == expected_msg


def test_set_message_body(chat_msg: ChatMessage):
    new_msg_body = "this is a new message in the chat"
    chat_msg.set_body(new_msg_body)
    assert chat_msg.body == new_msg_body


def test_set_nickname(chat_msg: ChatMessage):
    nickname = "karim_benzema"
    chat_msg.set_nickname(nickname)
    assert chat_msg.nick == nickname


def test_set_cmd_ok(chat_msg: ChatMessage):
    raw_cmd = "NOTICE #ayofishr :This room is now in unique-chat mode."
    chat_msg.set_command(raw_cmd)
    assert chat_msg.cmd == {"command": "NOTICE", "channel": "#ayofishr"}


def test_set_cmd_ignore(chat_msg: ChatMessage):
    raw_cmd = "ROOMSTATE #ayofishr :This room is now in unique-chat mode."
    chat_msg.set_command(raw_cmd)
    assert chat_msg.cmd == {}


def test_set_info(chat_msg: ChatMessage):
    raw_tags = "@badges=staff/1,broadcaster/1,turbo/1;color=#FF0000;display-name=PetsgomOO;emote-only=1;emotes=33:0-7;flags=0-7:A.6/P.6,25-36:A.1/I.2;id=c285c9ed-8b1b-4702-ae1c-c64d76cc74ef;mod=0;room-id=81046256;subscriber=1;turbo=0;tmi-sent-ts=1550868292494;user-id=81046256;user-type=staff :petsgomoo!petsgomoo@petsgomoo.tmi.twitch.tv PRIVMSG #petsgomoo :DansGame"
    chat_msg.set_info(raw_tags)

    assert chat_msg.color == "#FF0000"
    assert chat_msg.display_name == "PetsgomOO"
    assert chat_msg.subscriber == "1"
    assert chat_msg.timestamp == "20:44"


def test_handle_reconnect(chat_msg: ChatMessage, mocker):
    raw_cmd = "RECONNECT "
    chat_msg.set_command(raw_cmd)

    mock_rich_print = mocker.patch("rich.print")
    chat_msg.handle_cmd()
    mock_rich_print.assert_called_once_with(
        ":electric_plug:",
        "[bold red]The server needs to terminate the connection.[/bold red]",
    )


def test_handle_user_notice_cmd(chat_msg: ChatMessage, mocker):
    raw_cmd = "USERNOTICE #hello :something happened!"
    chat_msg.set_command(raw_cmd)

    mock_rich_print = mocker.patch("rich.print")
    chat_msg.handle_cmd()
    mock_rich_print.assert_called_once_with(
        ":celebrate:",
        f"[bold #32a369]somebody probably subbed ![/bold #32a369]",
    )


def test_whisper_cmd(chat_msg: ChatMessage, mocker):
    raw_cmd = "WHISPER foo :hello"
    chat_msg.set_command(raw_cmd)

    mock_rich_print = mocker.patch("rich.print")
    chat_msg.handle_cmd()
    mock_rich_print.assert_called_once_with(
        ":ninja:",
        f"[italic #c4c4c4]You received a whisper from a user. Go to twitch to reply.[/italic #c4c4c4]",
    )


def test_global_user_state_cmd(chat_msg: ChatMessage, mocker):
    raw_cmd = "GLOBALUSERSTATE "
    chat_msg.set_command(raw_cmd)

    mock_rich_print = mocker.patch("rich.print")
    chat_msg.handle_cmd()
    mock_rich_print.assert_called_once_with(
        ":white_check_mark:", "[bold green]We are authenticated[/bold green]"
    )


def test_user_notice_cmd(chat_msg: ChatMessage, mocker):
    raw_cmd = "NOTICE #bar :The message from foo is now deleted."
    chat_msg.set_command(raw_cmd)

    mock_rich_print = mocker.patch("rich.print")
    chat_msg.handle_cmd()
    mock_rich_print.assert_called_once_with(
        f"[INFO] - [italic #54f9d0]{chat_msg.body}[/italic #54f9d0]"
    )


def test_privmsg_cmd(chat_msg: ChatMessage, mocker):
    raw_cmd = "PRIVMSG #ayofishr :this is a custom message."
    chat_msg.set_command(raw_cmd)

    mock_rich_print = mocker.patch("rich.print")
    chat_msg.handle_cmd()
    mock_rich_print.assert_called_once_with(
        f"[#777777][{chat_msg.timestamp}][/#777777] [bold {chat_msg.color}]{chat_msg.nick}[/bold {chat_msg.color}]: {chat_msg.body}"
    )
