import pytest

from wofl.config import WoflConfig
from wofl.exceptions import MissingSectionError, MissingTokenError


def test_config_validate(parser):
    cfg = WoflConfig(parser=parser, path="./tests/test_data/config.ini")

    assert cfg.nickname == "ayofishr"
    assert cfg.token == "some-token"


def test_config_validate_missing_token(parser):
    with pytest.raises(MissingTokenError) as e:
        WoflConfig(parser=parser, path="./tests/test_data/missing_token_cfg.ini")

    assert e.type == MissingTokenError
    exception_msg = e.value.args[0]
    expected_error_msg = "[ERROR] - Missing key 'token' under [settings] section."
    assert exception_msg == expected_error_msg


def test_config_validate_wrong_path(parser):
    with pytest.raises(SystemExit) as e:
        WoflConfig(parser=parser, path="./this/path/does/not/exist/config.ini")
    assert e.type == SystemExit
    assert e.value.code == 1


def test_parse_config_missing_settings_section(parser):
    with pytest.raises(MissingSectionError) as e:
        WoflConfig(parser=parser, path="./tests/test_data/invalid_config.ini")
    assert e.type == MissingSectionError
    exception_msg = e.value.args[0]
    expected_error_msg = "[ERROR] - Section [settings] is missing from the config file."
    assert exception_msg == expected_error_msg
