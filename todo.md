# TODO

[] create CI/CD including linting and testing (and deploying)
[] figure out a way to remove the extra space when printing

## AT SOME POINT

[] try and authenticate with an invalid token and update tests accordingly
[] fix double print bug in NOTICE command ("ROOMSTATE is not being ignored")
_NOTE: use this URL to extract the relevant info https://dev.twitch.tv/docs/irc/tags/#privmsg-tags_
[] add a special emoji for subscriber emotes
[] add a special emoji for first time commenters
[] Highlight mod messages
[] handle bits cheer messages
[] highlight user-type messages (admin x staff x globalmod)
[] of course test all of them
[] handle command spefic tags (PRIVMSG,ROOMSTATE etc)
[] add a text box with Textual along with some nice styling
[] parse whisper message and extract the sender's username
[] add support for verbosity in the config
[] parse config as toml instead of ini

## DONE

[x] check that there is a token when parsing the config
[x] finish writing tests
[x] Write tests
[x] check that the file exists before proceeding
[x] move everything to the client class (Wofl -> `parse_msg` etc... )
[x] add a try/catch block for this monstrosity
[x] handle PONG
[x] implement the command related methods (`USERNOTICE` etc ...)
[x] Highlight links
[x] display the actual chat message
[x] Connect TO IRC
[x] Authenticate
[x] Join a channel
[x] Pull messages
[x] parse tags
[x] parse commands
[x] parse body
[x] parse nickname
[x] add a timestamp
