import pytest

from wofl.client import Wofl
from wofl.message import ChatMessage

# TODO: assert print statements with capsys


def test_wofl_connect(wofl: Wofl):
    wofl._connect()
    # NOTE: we access call_args[0][0] cause it's a nested tuple
    call_args = wofl.irc.connect.call_args[0][0]
    expected_call_args = (wofl.config.server, wofl.config.port)
    assert call_args == expected_call_args


def test_request_capabilities(wofl: Wofl):
    capabilities = " ".join(wofl.config.capabilities)

    wofl._request_capabilities()

    call_args = wofl.irc.send.call_args[0][0]
    expected_call_args = str.encode(f"CAP REQ :{capabilities}\n")
    assert call_args == expected_call_args


def test_authenticate(wofl: Wofl):
    wofl._authenticate()
    args_list = sorted(wofl.irc.send.call_args_list)
    nick_ = args_list[0].args[0]
    pass_ = args_list[1].args[0]
    assert wofl.irc.send.call_count == 2
    assert pass_ == str.encode(f"PASS oauth:{wofl.config.token}\n")
    assert nick_ == str.encode(f"NICK {wofl.config.nickname}\n")


def test_join(wofl: Wofl):
    wofl._join_channel()
    call_args = wofl.irc.send.call_args[0][0]
    expected_call_args = str.encode(f"JOIN #{wofl.config.nickname}\n")
    assert call_args == expected_call_args


def test_pong(wofl: Wofl):
    msg = "hello test"
    wofl._pong(msg)
    call_args = wofl.irc.send.call_args[0][0]
    expected_call_args = str.encode(f"PONG :{msg}")
    assert call_args == expected_call_args


def test_parse_msg(wofl: Wofl):
    test_msg = "@badge-info=;badges=broadcaster/1;client-nonce=28e05b1c83f1e916ca1710c44b014515;color=#0000FF;display-name=foofoo;emotes=62835:0-10;first-msg=0;flags=;id=f80a19d6-e35a-4273-82d0-cd87f614e767;mod=0;room-id=713936733;subscriber=1;tmi-sent-ts=1642696567751;turbo=0;user-id=713936733;user-type= :foofoo!foofoo@foofoo.tmi.twitch.tv PRIVMSG #bar :bleedPurple"
    wofl._parse_msg(str.encode(test_msg))

    assert wofl.msg.mod == "0"
    assert wofl.msg.display_name == "foofoo"
    assert wofl.msg.subscriber == "1"
    assert wofl.msg.user_type == ""
    assert wofl.msg.nick == "foofoo"
    assert wofl.msg.body == "bleedPurple"
    assert wofl.msg.color == "#0000FF"
    assert wofl.msg.timestamp == "16:36"


def test_parse_msg_false(wofl: Wofl):
    with pytest.raises(IndexError) as e:
        wofl._parse_msg(str.encode(""))

    assert e.type == IndexError


def test_public_connect(wofl: Wofl, mocker):
    mock_connect = mocker.patch.object(wofl, "_connect")
    mock_req_cap = mocker.patch.object(wofl, "_request_capabilities")
    mock_auth = mocker.patch.object(wofl, "_authenticate")
    mock_join = mocker.patch.object(wofl, "_join_channel")

    wofl.connect()

    assert mock_connect.called is True
    assert mock_req_cap.called is True
    assert mock_auth.called is True
    assert mock_join.called is True


def test_run_ping(wofl: Wofl, mocker):
    ping_msg = "tmi.twitch.tv"
    wofl.irc.recv.return_value = str.encode(
        f":foo!foo@foo.tmi.twitch.tv PING :{ping_msg}"
    )
    mock_pong = mocker.patch.object(wofl, "_pong")
    wofl.run()
    assert mock_pong.called is True
    assert mock_pong.call_args_list[0].args[0] == ping_msg


def test_run(wofl: Wofl, mocker):
    test_msg = "@badge-info=;badges=broadcaster/1;client-nonce=28e05b1c83f1e916ca1710c44b014515;color=#0000FF;display-name=foofoo;emotes=62835:0-10;first-msg=0;flags=;id=f80a19d6-e35a-4273-82d0-cd87f614e767;mod=0;room-id=713936733;subscriber=1;tmi-sent-ts=1642696567751;turbo=0;user-id=713936733;user-type= :foofoo!foofoo@foofoo.tmi.twitch.tv PRIVMSG #bar :bleedPurple"
    wofl.irc.recv.return_value = str.encode(test_msg)
    mock_msg_handle_cmd = mocker.patch.object(wofl.msg, "handle_cmd")
    wofl.run()
    assert mock_msg_handle_cmd.called is True
