import configparser
import socket
import time

from wofl.config import WoflConfig
from wofl.message import ChatMessage


class Wofl:
    def __init__(self, cfg: WoflConfig) -> None:
        self.config = cfg
        self.irc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.msg = ChatMessage()

    def _parse_msg(self, raw_msg: bytes):
        idx = 0
        str_msg = raw_msg.decode("UTF-8")

        # parse message tags
        if str_msg[idx] == "@":
            end_idx = str_msg.find(" ")
            raw_tags = str_msg[:end_idx]
            self.msg.set_info(raw_tags)
            idx = end_idx + 1

        # parse message source
        if str_msg[idx] == ":":
            idx += 1
            end_idx = str_msg.find(" ", idx)
            raw_msg_source = str_msg[idx:end_idx]
            self.msg.set_nickname(raw_msg_source)
            idx = end_idx + 1

        # parse message command
        end_idx = str_msg.find(":", idx)
        command = str_msg[idx:end_idx]

        self.msg.set_command(command)

        # parse message body
        if end_idx != len(str_msg):
            idx = end_idx + 1
            body = str_msg[idx:]
            self.msg.set_body(body)

    def _connect(self):
        print(f"Connecting to {self.config.server} on port {self.config.port} ...")
        self.irc.connect((self.config.server, self.config.port))

    def _request_capabilities(self):
        caps = " ".join(self.config.capabilities)
        print(f"requesting capabilities ... {caps}")

        self.irc.send(str.encode(f"CAP REQ :{caps}\n"))

    def _authenticate(self):
        print(f"Authenticating as {self.config.nickname} ...")
        self.irc.send(str.encode(f"PASS oauth:{self.config.token}\n"))
        self.irc.send(str.encode(f"NICK {self.config.nickname}\n"))

    def _join_channel(self):
        self.irc.send(str.encode(f"JOIN #{self.config.nickname}\n"))

    def _pong(self, msg_body: str):
        self.irc.send(str.encode(f"PONG :{msg_body}"))

    def connect(self):
        self._connect()
        self._request_capabilities()
        self._authenticate()
        self._join_channel()

    def run(self):
        text = self.irc.recv(2048)
        try:
            self._parse_msg(text)
        except IndexError:
            print("[DEBUG] - Could not parse message from twitch.")
        else:
            if self.msg.cmd.get("command") == "PING":
                self._pong(self.msg.body)
            else:
                self.msg.handle_cmd()


def main():
    parser = configparser.ConfigParser()

    config = WoflConfig(parser=parser)
    wofl = Wofl(config)

    while 1:
        time.sleep(1)
        wofl.run()


if __name__ == "__main__":
    main()
