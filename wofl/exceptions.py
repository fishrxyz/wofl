class MissingSectionError(Exception):
    def __init__(self, section: str) -> None:
        self.section = section
        self.msg = (
            f"[ERROR] - Section [{self.section}] is missing from the config file."
        )
        Exception.__init__(self, self.msg)


class MissingTokenError(Exception):
    def __init__(self) -> None:
        Exception.__init__(
            self, "[ERROR] - Missing key 'token' under [settings] section."
        )
